require 'sinatra/base'
require 'haml'
require 'data_mapper'
require 'json'

class PieRatBay < Sinatra::Base
  use Rack::MethodOverride
  set :root, File.expand_path('../../', __FILE__)

  DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite:data")
  Dir["#{root}/models/*"].each {|file| require file }
  DataMapper.finalize
  DataMapper.auto_upgrade!

  get '/' do
    haml :index
  end

  get '/list' do
    { :res => Torrent.find_page(params[:q] || '', params[:page].to_i || 0),
      :pages => Torrent.page_count(params[:q] || '')}.to_json
  end
end
