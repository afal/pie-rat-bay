$(function () {
    var get_data = function (q, page) {
        $.getJSON('/list', {q:q, page:page}, function (res) {
            $('#torrents').html(ich.torrents_table({torrents: res.res}));
            var pages = {'page': +page + 1, 'search': q, 'total_pages': res.pages};
            if (~~page > 0) {
                pages['page_previous'] = (+page - 1) + "";
            }
            if (+page + 1 !== res.pages) {
                pages['page_next'] = +page + 1;
            }
            $('#pages').html(ich.pagination(pages));
        });
    };

    var bix = Bix({
        '/': function () {
            get_data('', 0);
        },
        '/:search/:page': function (search, page) {
            get_data(search, page);
        }
    });

    bix.config({
        forceHash: true
    });

    bix.run();

    $('#search_box').keyup(function () {
        location.hash = '#/' + this.value + '/0';
    });
});