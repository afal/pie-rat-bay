class Torrent
  include DataMapper::Resource

  property :id,        Integer, :key => true
  property :title,     String, :length => 150 
  property :size,      Integer
  property :seeders,   Integer
  property :leechers,  Integer
  property :magnet,    String, :length => 50

  def self.find_page(q, page)
    per_page = 20
    offset = per_page * page
    self.all(:title.like=>"%#{q}%", :offset=>offset, :limit=>per_page)
  end

  def self.page_count(q)
    (self.count(:title.like=>"%#{q}%") / 20.0).ceil
  end
end